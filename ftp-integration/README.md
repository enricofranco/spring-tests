# Spring FTP Integration

It is possible to run a local FTP server with command

```shell
> docker-compose up -d
```

This FTP server can be accessed on `localhost` through standard port `21` with:

- Username `test`
- Password `test`

## Source

- [Spring Tips: Remote File System Integrations (FTP) with Spring Integration](https://spring.io/blog/2020/03/18/spring-tips-remote-file-system-integrations-ftp-with-spring-integration)
