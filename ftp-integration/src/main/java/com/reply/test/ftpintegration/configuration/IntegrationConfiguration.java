package com.reply.test.ftpintegration.configuration;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.MessageChannels;
import org.springframework.integration.event.inbound.ApplicationEventListeningMessageProducer;
import org.springframework.integration.ftp.server.ApacheMinaFtpEvent;
import org.springframework.integration.ftp.server.ApacheMinaFtplet;
import org.springframework.integration.ftp.session.DefaultFtpSessionFactory;
import org.springframework.integration.handler.GenericHandler;
import org.springframework.messaging.MessageChannel;

@Log4j2
@Configuration
@Profile("unused")
public class IntegrationConfiguration {

    @Bean
    DefaultFtpSessionFactory defaultFtpSessionFactory(
            @Value("${ftp.username}") String username,
            @Value("${ftp.password}") String password,
            @Value("${ftp.host}") String host,
            @Value("${ftp.port}") int port) {

        DefaultFtpSessionFactory defaultFtpSessionFactory = new DefaultFtpSessionFactory();
        defaultFtpSessionFactory.setPassword(password);
        defaultFtpSessionFactory.setUsername(username);
        defaultFtpSessionFactory.setHost(host);
        defaultFtpSessionFactory.setPort(port);
        return defaultFtpSessionFactory;
    }

    @Bean
    ApacheMinaFtplet apacheMinaFtplet() {
        return new ApacheMinaFtplet();
    }

    @Bean
    MessageChannel messageChannel() {
        return MessageChannels.direct().get();
    }

    @Bean
    IntegrationFlow integrationFlow() {
        return IntegrationFlows.from(this.messageChannel())
                .handle((GenericHandler<ApacheMinaFtpEvent>) (apacheMinaFtpEvent, messageHeaders) -> {
                    log.info("New event received {}:{}",
                            apacheMinaFtpEvent.getClass().getName(),
                            apacheMinaFtpEvent.getSession());
                    return null;
                })
                .get();
    }

    @Bean
    ApplicationEventListeningMessageProducer applicationEventListeningMessageProducer() {
        ApplicationEventListeningMessageProducer producer = new ApplicationEventListeningMessageProducer();
        producer.setEventTypes(ApacheMinaFtpEvent.class);
        producer.setOutputChannel(messageChannel());
        return producer;
    }
}
