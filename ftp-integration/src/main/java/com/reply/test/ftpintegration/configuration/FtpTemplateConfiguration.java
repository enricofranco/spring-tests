package com.reply.test.ftpintegration.configuration;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.integration.ftp.session.DefaultFtpSessionFactory;
import org.springframework.integration.ftp.session.FtpRemoteFileTemplate;

import java.io.File;
import java.io.FileOutputStream;

@Log4j2
@Configuration
@Profile("unused")
public class FtpTemplateConfiguration {

    @Bean
    InitializingBean initializingBean(FtpRemoteFileTemplate template) {
        return () -> template.execute(session -> {
            File file = new File(new File(System.getProperty("user.dir"), "Downloads"), "hello-local.txt");
            try (FileOutputStream fOut = new FileOutputStream(file)) {
                session.read("hello-world.txt", fOut);
            }
            log.info("Read {}", file.getAbsolutePath());
            return null;
        });
    }

    @Bean
    DefaultFtpSessionFactory defaultFtpSessionFactory(
            @Value("${ftp.username}") String username,
            @Value("${ftp.password}") String password,
            @Value("${ftp.host}") String host,
            @Value("${ftp.port}") int port) {

        DefaultFtpSessionFactory defaultFtpSessionFactory = new DefaultFtpSessionFactory();
        defaultFtpSessionFactory.setUsername(username);
        defaultFtpSessionFactory.setPassword(password);
        defaultFtpSessionFactory.setHost(host);
        defaultFtpSessionFactory.setPort(port);
        defaultFtpSessionFactory.setClientMode(FTPClient.PASSIVE_LOCAL_DATA_CONNECTION_MODE);
        return defaultFtpSessionFactory;
    }

    @Bean
    FtpRemoteFileTemplate ftpRemoteFileTemplate(DefaultFtpSessionFactory sessionFactory) {
        return new FtpRemoteFileTemplate(sessionFactory);
    }
}
