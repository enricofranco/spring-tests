package com.reply.test.ftpintegration.configuration;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.ftp.dsl.Ftp;
import org.springframework.integration.ftp.dsl.FtpInboundChannelAdapterSpec;
import org.springframework.integration.ftp.session.DefaultFtpSessionFactory;
import org.springframework.integration.handler.GenericHandler;
import org.springframework.util.Assert;

import java.io.File;
import java.util.concurrent.TimeUnit;

@Log4j2
@Configuration
public class InboundConfiguration {

    @Bean
    DefaultFtpSessionFactory defaultFtpSessionFactory(
            @Value("${ftp.username}") String username,
            @Value("${ftp.password}") String password,
            @Value("${ftp.host}") String host,
            @Value("${ftp.port}") int port) {

        DefaultFtpSessionFactory defaultFtpSessionFactory = new DefaultFtpSessionFactory();
        defaultFtpSessionFactory.setPassword(password);
        defaultFtpSessionFactory.setUsername(username);
        defaultFtpSessionFactory.setHost(host);
        defaultFtpSessionFactory.setPort(port);
        defaultFtpSessionFactory.setClientMode(FTPClient.PASSIVE_LOCAL_DATA_CONNECTION_MODE);
        return defaultFtpSessionFactory;
    }

    @Bean
    IntegrationFlow inboundFlow(DefaultFtpSessionFactory sessionFactory) {
        File localDirectory = new File(new File(System.getProperty("user.dir"), "Downloads"), "local");
        Assert.isTrue(localDirectory.exists() || localDirectory.mkdirs(), "The directory could not be created and does not exist");
        FtpInboundChannelAdapterSpec spec = Ftp.inboundAdapter(sessionFactory)
                .autoCreateLocalDirectory(true)
                .patternFilter("*.txt")
                .localDirectory(localDirectory);
        return IntegrationFlows
                .from(spec, pc -> pc.poller(pm -> pm.fixedRate(1000, TimeUnit.MILLISECONDS)))
                .handle((GenericHandler<File>) (file, messageHeaders) -> {
                    log.info("New file: {}", file.getAbsolutePath());
                    messageHeaders.forEach((k, v) -> log.info("{}:{}", k, v));
                    return null;
                })
                .get();
    }
}
