package com.reply.test.rabbitmq;

import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    @Value("${rabbitmq.queue}")
    private String QUEUE_NAME;

    @Bean
    public Queue hello() {
        return new Queue(QUEUE_NAME);
    }
}
