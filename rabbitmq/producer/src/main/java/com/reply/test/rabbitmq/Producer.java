package com.reply.test.rabbitmq;

import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class Producer {

    private final RabbitTemplate rabbitTemplate;
    private final Queue queue;
    private long id = 0L;

    @Autowired
    public Producer(RabbitTemplate rabbitTemplate, Queue queue) {
        this.rabbitTemplate = rabbitTemplate;
        this.queue = queue;
    }

    @Scheduled(initialDelayString = "${scheduling.initial:500}", fixedDelayString = "${scheduling.fixed:1000}")
    public void send() {
        final Item item = new Item("Hello", ++id);
        this.rabbitTemplate.convertAndSend(queue.getName(), item);
        log.info("Sent object <{}> in queue {}", item, queue.getName());
    }
}
