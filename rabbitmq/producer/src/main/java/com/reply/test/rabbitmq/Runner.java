package com.reply.test.rabbitmq;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class Runner implements CommandLineRunner {

    private final int duration;
    private final ConfigurableApplicationContext ctx;

    public Runner(@Value("${duration:0}") int duration,
                  ConfigurableApplicationContext ctx) {
        this.duration = duration;
        this.ctx = ctx;
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("Ready and running for {} ms", duration);
        Thread.sleep(duration);
        ctx.close();
    }
}
