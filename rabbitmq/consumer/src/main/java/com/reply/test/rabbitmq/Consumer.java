package com.reply.test.rabbitmq;

import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "${rabbitmq.queue}")
@Log4j2
public class Consumer {

    @RabbitHandler
    public void receive(Item input) {
        log.info("Received message <{}>", input);
    }
}
