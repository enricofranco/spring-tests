# Spring Batch

H2 Database at `localhost:8080/h2-console` with JDBC URL: `jdbc:h2:mem:batchdb`

- Username `sa`
- Password empty

Job flow can be activated with

- `HTTP GET` request on `localhost:8080/invoke/{input_file}`
- Messages on RabbitMQ queue configuration in file `application.properties` with property `rabbitmq.queue`

## Structure of a RabbitMQ message

Example of a message:

- Exchange: AMQP default
- Routing Key: `batch-activator`
- Properties
  - priority: `0`
  - delivery_mode: `2`
  - headers:
  - content_encoding: `UTF-8`
  - content_type: `text/plain`
- Payload (11 bytes - Encoding: `string`): `input00.csv`
