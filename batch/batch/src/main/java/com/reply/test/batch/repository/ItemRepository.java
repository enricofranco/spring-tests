package com.reply.test.batch.repository;

import com.reply.test.batch.model.Item;
import org.springframework.data.repository.CrudRepository;

public interface ItemRepository extends CrudRepository<Item, Long> {
}
