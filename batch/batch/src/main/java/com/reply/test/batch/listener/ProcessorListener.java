package com.reply.test.batch.listener;

import com.reply.test.batch.model.OriginalItem;
import com.reply.test.batch.model.TransformedItem;
import lombok.extern.log4j.Log4j2;
import org.springframework.batch.core.ItemProcessListener;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class ProcessorListener implements ItemProcessListener<OriginalItem, TransformedItem> {

    @Override
    public void beforeProcess(OriginalItem originalItem) {
        log.info("Before processing");
    }

    @Override
    public void afterProcess(OriginalItem originalItem, TransformedItem transformedItem) {
        log.info("After processing");
    }

    @Override
    public void onProcessError(OriginalItem originalItem, Exception e) {
        log.info("Error processing");
    }
}
