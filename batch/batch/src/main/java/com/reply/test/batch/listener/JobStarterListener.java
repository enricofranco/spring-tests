package com.reply.test.batch.listener;

import com.reply.test.batch.config.Keywords;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "${rabbitmq.queue}")
@Log4j2
public class JobStarterListener {

    private final JobLauncher jobLauncher;
    private final Job job;

    @Autowired
    public JobStarterListener(JobLauncher jobLauncher, Job job) {
        this.jobLauncher = jobLauncher;
        this.job = job;
    }

    @RabbitHandler
    public void receive(String fileName) throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {
        log.info("Received message <{}>", fileName);

        JobParameters jobParameters = new JobParametersBuilder()
                .addLong(Keywords.KEYWORD_TIMESTAMP, System.currentTimeMillis())
                .addString(Keywords.KEYWORD_FILENAME, fileName)
                .toJobParameters();
        jobLauncher.run(job, jobParameters);

        log.info("Job {} with filename {} returned", job.getName(), fileName);
    }
}
