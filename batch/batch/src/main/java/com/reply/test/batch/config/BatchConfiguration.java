package com.reply.test.batch.config;

import com.reply.test.batch.model.OriginalItem;
import com.reply.test.batch.model.TransformedItem;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;

    @Autowired
    public BatchConfiguration(JobBuilderFactory jobBuilderFactory,
                              StepBuilderFactory stepBuilderFactory) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
    }

    @Bean
    public Job importItemJob(JobExecutionListener listener, Step step1) {
        return jobBuilderFactory.get("importJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(step1)
                .end()
                .build();
    }

    @Bean
    public Step firstStep(ItemReader<OriginalItem> reader,
                          ItemProcessor<OriginalItem, TransformedItem> processor,
                          ItemWriter<TransformedItem> writer,
                          ItemReadListener<OriginalItem> readerListener,
                          ItemProcessListener<OriginalItem, TransformedItem> processorListener,
                          ItemWriteListener<TransformedItem> writerListener) {
        return stepBuilderFactory.get("myFirstSequence")
                .<OriginalItem, TransformedItem>chunk(1)
                .reader(reader)
                .processor(processor)
                .writer(writer)
                .listener(readerListener)
                .listener(processorListener)
                .listener(writerListener)
                .build();
    }

    @Bean
    @StepScope
    public FlatFileItemReader<OriginalItem> reader(
            @Value("#{jobParameters[filename]}") String filename) {
        return new FlatFileItemReaderBuilder<OriginalItem>()
                .name("csvReader")
                .linesToSkip(1)
                .resource(new ClassPathResource(filename))
                .delimited()
                .names("name", "price", "discount")
                .fieldSetMapper(new BeanWrapperFieldSetMapper<>() {{
                    setTargetType(OriginalItem.class);
                }})
                .build();
    }
}
