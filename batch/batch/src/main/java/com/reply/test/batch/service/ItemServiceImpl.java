package com.reply.test.batch.service;

import com.reply.test.batch.model.Item;
import com.reply.test.batch.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class ItemServiceImpl implements ItemService {

    private final ItemRepository itemRepository;

    @Autowired
    public ItemServiceImpl(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @Override
    public List<Item> saveItems(List<Item> items) {
        Iterable<Item> savedIterables = itemRepository.saveAll(items);
        return StreamSupport.stream(savedIterables.spliterator(), false)
                .collect(Collectors.toList());
    }
}
