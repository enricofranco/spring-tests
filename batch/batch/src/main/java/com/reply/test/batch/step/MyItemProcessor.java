package com.reply.test.batch.step;

import com.reply.test.batch.model.OriginalItem;
import com.reply.test.batch.model.TransformedItem;
import lombok.extern.log4j.Log4j2;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class MyItemProcessor implements ItemProcessor<OriginalItem, TransformedItem> {

    @Override
    public TransformedItem process(final OriginalItem originalItem) {
        TransformedItem transformedItem = TransformedItem.builder()
                .name(originalItem.getName().toUpperCase())
                .finalPrice(performSomeComputation(originalItem.getPrice(), originalItem.getDiscount()))
                .build();
        log.info("Transformed <{}> into <{}>", originalItem, transformedItem);
        return transformedItem;
    }

    private double performSomeComputation(final double price, final double discount) {
        return price * (100 - discount) / 100;
    }
}
