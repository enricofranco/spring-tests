package com.reply.test.batch.step;

import com.reply.test.batch.model.Item;
import com.reply.test.batch.model.TransformedItem;
import com.reply.test.batch.service.ItemService;
import lombok.extern.log4j.Log4j2;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@Log4j2
public class Writer implements ItemWriter<TransformedItem> {

    private final ItemService itemService;

    @Autowired
    public Writer(ItemService itemService) {
        this.itemService = itemService;
    }

    @Override
    public void write(List<? extends TransformedItem> transformedItems) {
        List<Item> items = transformedItems.stream()
                .peek(i -> log.info("Writer got <{}>", i))
                .map(Item::fromTransformedItem)
                .collect(Collectors.toList());
        itemService.saveItems(items)
                .forEach(i -> log.info("Writer saved <{}>", i));
    }
}
