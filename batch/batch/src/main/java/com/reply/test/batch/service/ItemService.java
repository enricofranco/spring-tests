package com.reply.test.batch.service;

import com.reply.test.batch.model.Item;

import java.util.List;

public interface ItemService {

    List<Item> saveItems(List<Item> items);
}
