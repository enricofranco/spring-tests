package com.reply.test.batch.controller;

import com.reply.test.batch.config.Keywords;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class JobInvokerController {

    private final JobLauncher jobLauncher;
    private final Job job;

    @Autowired
    public JobInvokerController(JobLauncher jobLauncher, Job job) {
        this.jobLauncher = jobLauncher;
        this.job = job;
    }

    @GetMapping("/invoke/{fileName}")
    public String invokeJob(@PathVariable String fileName)
            throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {

        JobParameters jobParameters = new JobParametersBuilder()
                .addLong(Keywords.KEYWORD_TIMESTAMP, System.currentTimeMillis())
                .addString(Keywords.KEYWORD_FILENAME, fileName)
                .toJobParameters();
        jobLauncher.run(job, jobParameters);
        return "Job Started";
    }
}
