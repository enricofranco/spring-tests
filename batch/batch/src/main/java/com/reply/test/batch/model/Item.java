package com.reply.test.batch.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private Double price;

    public static Item fromTransformedItem(final TransformedItem input) {
        return Item.builder()
                .name(input.getName())
                .price(input.getFinalPrice())
                .build();
    }
}
