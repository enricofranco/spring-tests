package com.reply.test.batch.listener;

import com.reply.test.batch.model.OriginalItem;
import lombok.extern.log4j.Log4j2;
import org.springframework.batch.core.ItemReadListener;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class ReaderListener implements ItemReadListener<OriginalItem> {

    @Override
    public void beforeRead() {
        log.info("Before reading");
    }

    @Override
    public void afterRead(OriginalItem originalItem) {
        log.info("After reading");
    }

    @Override
    public void onReadError(Exception e) {
        log.error("Error reading");
    }
}
