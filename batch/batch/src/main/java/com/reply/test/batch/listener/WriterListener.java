package com.reply.test.batch.listener;

import com.reply.test.batch.model.TransformedItem;
import lombok.extern.log4j.Log4j2;
import org.springframework.batch.core.ItemWriteListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Log4j2
public class WriterListener implements ItemWriteListener<TransformedItem> {

    @Override
    public void beforeWrite(List<? extends TransformedItem> list) {
        log.info("Before writing");
    }

    @Override
    public void afterWrite(List<? extends TransformedItem> list) {
        log.info("After writing");
    }

    @Override
    public void onWriteError(Exception e, List<? extends TransformedItem> list) {
        log.info("Error writing");
    }
}
