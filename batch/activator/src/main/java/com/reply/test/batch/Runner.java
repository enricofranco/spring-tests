package com.reply.test.batch;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class Runner implements CommandLineRunner {

    private final int duration;
    private final ConfigurableApplicationContext ctx;

    public Runner(@Value("${duration:5000}") int duration,
                  ConfigurableApplicationContext ctx) {
        this.duration = duration;
        this.ctx = ctx;
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("Ready and running for {} ms", duration);
        Thread.sleep(duration);
        ctx.close();
    }
}
