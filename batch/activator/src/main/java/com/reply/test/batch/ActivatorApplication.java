package com.reply.test.batch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ActivatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(ActivatorApplication.class, args);
    }

}
