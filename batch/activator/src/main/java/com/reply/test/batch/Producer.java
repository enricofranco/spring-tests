package com.reply.test.batch;

import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class Producer {

    private final RabbitTemplate rabbitTemplate;
    private final Queue queue;
    private int id = 0;
    private static final int LENGTH = 3;

    @Autowired
    public Producer(RabbitTemplate rabbitTemplate, Queue queue) {
        this.rabbitTemplate = rabbitTemplate;
        this.queue = queue;
    }

    @Scheduled(initialDelayString = "${scheduling.initial:500}", fixedDelayString = "${scheduling.fixed:1000}")
    public void send() {
        final String filename = String.format("input%02d.csv", id);
        this.rabbitTemplate.convertAndSend(queue.getName(), filename);
        log.info("Sent message <{}> in queue {}", filename, queue.getName());
        id = (id + 1) % LENGTH;
    }
}
