# Spring Tests

This repository contains my works useful to study and learn some advanced features of Spring framework.

- Directory **rabbitmq** contains a basic producer-consumer scheme which exploits RabbitMQ
- Directory **batch** contains a little more complex project using Spring Batch

## RabbitMQ

It is possible to run a local RabbitMQ with command

```shell
> docker-compose up -d
```

Some standard ports will be open on the system, in order to reach RabbitMQ from `localhost`:

- Standard port `5672` to reach RabbitMQ Broker
- Standard port `15672` to reach management GUI from a web browser
  - Username: `guest`
  - Password: `guest`
