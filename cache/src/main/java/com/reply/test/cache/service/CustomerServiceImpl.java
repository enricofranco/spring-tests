package com.reply.test.cache.service;

import com.reply.test.cache.model.Customer;
import lombok.extern.log4j.Log4j2;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Log4j2
@Service
@CacheConfig(cacheNames = "customers")
public class CustomerServiceImpl implements CustomerService {

    private final Map<Long, Customer> customers;

    public CustomerServiceImpl() {
        this.customers = new HashMap<>();
        customers.put(1L, new Customer(1L, "First customer"));
        customers.put(2L, new Customer(2L, "Second customer"));
        customers.put(3L, new Customer(3L, "Third customer"));
    }

    @Cacheable
    @Override
    public Customer getCustomer(final Long id) {
        log.info("Retrieving customer with id {}", id);
        return customers.get(id);
    }
}
