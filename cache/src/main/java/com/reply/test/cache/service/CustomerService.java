package com.reply.test.cache.service;

import com.reply.test.cache.model.Customer;

public interface CustomerService {

    Customer getCustomer(Long id);
}
