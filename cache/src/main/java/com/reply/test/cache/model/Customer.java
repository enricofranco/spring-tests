package com.reply.test.cache.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@ToString
public class Customer {

    private Long id;
    private String name;
}
